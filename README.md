# Muon Collider Tracking Scripts

A series of Python scripts to make performance plots related to tracking at a Muon Collider.

## Installation
The following creates a virtual environment and setups the plotting code inside of it.
```shell
python3 -m venv path/to/myenv
source path/to/myenv/activate
git clone https://gitlab.cern.ch/berkeleylab/MuonCollider/mccplots.git
cd mccplots
pip install -e .
```

Don't forget to load the virtual environment every session
```shell
source path/to/myenv/activate
```

## Running

### Data Sample Definitions
All samples should be defined inside the `data.yaml` file. It is loaded inside plotting scripts using the `data.dm.DataManager` class.

The following keys are expected in the YAML file.
- `datadir`: Location of the samples on the local computer. (*Update value to match your setup!!!*) 
- `loader`: Class to use for loading samples.
- `samples`: List of available samples.

Samples (entries in `samples`) are listed as key:value pairs where the key is the name used to refer to the sample and value is a dictionary with the following entries.
- `file`: Sample data location inside `datadir`.
- `title`: Text to display in plots (ie: legend).


Example `data.yaml`.
```yaml
datadir:  '/ssdisk02/kkrizka/MuonCollider/output'
loader: 'muc.lcparquet.LCParquet'
samples:
  marlintruth:
    file: 'data_marlintruth'
    title: 'Truth Trk (Marlin)'
```

Example usage in a plotting script.
```python
import data.dm as dm
data=dm.DataManager('data.yaml')
data.samples['marlintruth'] # instance of LCParquet class
```

### Plotting Scripts
All scripts to make plots are in the root directory and are prefixed with `plot_`.

Most scripts contain the following lines used to configure the samples that are included in the generated plots. They are meant to be edited.
```python
samples=['actstruth','actsckf','actsseed','actsckfbib']
```
Example running
```shell
python plot_trkeff.py
```

#### plot_trkeff.py
Make a plot of track reconstruction efficiency vs different properties (ie: muon pT).

#### plot_trkhits.py
Make plots of quantities related to hits associated to fitted tracks. They include:
- Overall hits per track.
- Hits per track vs lambda.
- Hit position pull plots in different layers.

#### plot_trkpulls.py
Make plots of track parameter pulls.
