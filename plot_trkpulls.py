#
# Plot tracking efficiency as a function of random variables

import mcc.lcparquet as lcpq
import data.dm as dm

import matplotlib.pyplot as plt
import dask.array as da
import pandas as pd
import numpy as np

data=dm.DataManager('data.yaml')

#
# Load all necessary dataframes
samples=['actstruth','marlintruth']

mc=lcpq.concat_load([data.samples[sample] for sample in samples], 'mc')
tr=lcpq.concat_load([data.samples[sample] for sample in samples], 'tr')
ts=lcpq.concat_load([data.samples[sample] for sample in samples], 'ts')

mc2tr=lcpq.concat_load([data.samples[sample] for sample in samples], 'mc2tr')

# the single muon
mu=mc[mc.colidx==0]

# merge to get all info in one dataframe
tracks=mu.merge(
    mc2tr,left_on=['title','evt','colidx'],right_on=['title','evt','from']).merge(
        tr,left_on=['title','evt','to'],right_on=['title','evt','colidx']).merge(
            ts,left_on=['title','evt','sip'],right_on=['title','evt','colidx'], suffixes=('_mc','_tr'))

titles=sorted(tracks.title.unique().compute())

# Pulls
tracks['mot_unc']=da.sqrt(tracks.covmotmot)
tracks['mot_pull']=(tracks.mot_tr-tracks.mot_mc)/tracks.mot_unc

tracks['phi_unc']=da.sqrt(tracks.covphiphi)
tracks['phi_pull']=(tracks.phi_tr-tracks.phi_mc)/tracks.phi_unc

tracks['lambda_unc']=da.sqrt(tracks.covlamlam)
tracks['lambda_pull']=(tracks.lambda_tr-tracks.lambda_mc)/tracks.lambda_unc

tracks['zze_unc']=da.sqrt(tracks.covzzezze)
tracks['zze_tr']=tracks.zze
tracks['zze_mc']=tracks.vtz
tracks['zze_pull']=(tracks.zze_tr)/tracks.zze_unc

tracks['dze_unc']=da.sqrt(tracks.covdzedze)
tracks['dze_tr']=tracks.dze
tracks['dze_mc']=da.sqrt(tracks.vtx*tracks.vtx+tracks.vty*tracks.vty)
tracks['dze_pull']=(tracks.dze_tr)/tracks.dze_unc

tracks['lambda_bin']=tracks['lambda_mc'].map_partitions(pd.cut, bins=np.arange(-3.14/2,3.15/2,0.1))

#
# Plot stuff
histcommon={'histtype':'step', 'density':True}

# Define variables to plot pulls of
variables=[
    {
        'name':'mot',
        'title':'$p_{T}$',
        'range':0.1,
        'units':'GeV'
    },
    {
        'name':'lambda',
        'title':'$\lambda$',
        'range':0.005,
        'units':'rad'
    },
    {
        'name':'phi',
        'title':'$\phi$',
        'range':0.05,
        'units':'rad'
    },
    {
        'name':'dze',
        'title':'$d_0$',
        'range':0.1,
        'units':'mm'
    },
    {
        'name':'zze',
        'title':'$z_0$',
        'range':0.1,
        'units':'mm'
    },
]

for var in variables:
    # Residuals
    range_resid=(-var['range'],var['range'])
    for title in titles:
        mytracks=tracks[tracks.title==title]

        resid=mytracks['{}_tr'.format(var['name'])]-mytracks['{}_mc'.format(var['name'])]

        resid,bins=da.histogram(resid, bins=100, range=range_resid)
        hist=da.compute(resid)

        plt.hist(bins[:-1], bins, weights=hist, label=f'{title}', **histcommon)
    plt.xlabel('Track {} Residual [{}]'.format(var['title'],var['units']))
    plt.xlim(range_resid)
    plt.legend(loc='upper left')
    plt.tight_layout()
    plt.savefig('resid_{}.png'.format(var['name']))
    plt.show()
    plt.clf()

    # Uncertainties
    name_unc='{}_unc'.format(var['name'])
    range_unc=(0,var['range'])
    for title in titles:
        mytracks=tracks[tracks.title==title]

        unc=mytracks[name_unc]
        unc,bins=da.histogram(unc, bins=100, range=range_unc)
        hist=da.compute(unc)

        plt.hist(bins[:-1], bins, weights=hist, label=f'{title}', **histcommon)
    plt.xlabel('Track {} Uncertainty [{}]'.format(var['title'],var['units']))
    plt.xlim(range_unc)
    plt.legend(loc='upper right')
    plt.tight_layout()
    plt.savefig('unc_{}.png'.format(var['name']))
    plt.show()
    plt.clf()

    # Pull plot
    for title in titles:
        mytracks=tracks[tracks.title==title]

        cpull='{}_pull'.format(var['name'])
        fpullcore=da.fabs(mytracks[cpull])<3

        pull,bins=da.histogram(mytracks[cpull], bins=20, range=(-5,5))
        pull,mean,sigma=da.compute(pull,mytracks[fpullcore][cpull].mean(),mytracks[fpullcore][cpull].std())
    
        plt.hist(bins[:-1], bins, weights=pull, label=f'{title}, {mean:0.2f}$\pm${sigma:0.2f}', **histcommon)
    plt.xlabel('Track {} Pull'.format(var['title']))
    plt.xlim(-5,5)
    plt.ylim(0,0.75)
    plt.legend(loc='upper left')
    plt.tight_layout()
    plt.savefig('pull_{}.png'.format(var['name']))
    plt.show()
    plt.clf()
    
    # vs lambda
    for title in titles:
        mytracks=tracks[tracks.title==title]

        df=mytracks.groupby('lambda_bin')[['lambda_mc',name_unc]].mean()

        x,y=da.compute(df.lambda_mc, df[name_unc])
        plt.plot(x,y,'.-',label=f'{title}')

    plt.xlabel('Muon $\lambda$')
    plt.ylabel('<Track {} Uncertainty> [{}]'.format(var['title'],var['units']))
    #plt.xlim(-5,5)
    plt.ylim(range_unc)
    plt.legend(loc='upper left')
    plt.tight_layout()
    plt.savefig('vslambda_{}.png'.format(var['name']))
    plt.show()
    plt.clf()
    
