#
# Plot tracking efficiency as a function of random variables

import mcc.lcparquet as lcpq
import data.dm as dm

import matplotlib.pyplot as plt
import dask.array as da
import pandas as pd
import numpy as np

data=dm.DataManager('data.yaml')

#
# Load all necessary dataframes
samples=['marlintruth','actstruth']

mc=lcpq.concat_load([data.samples[sample] for sample in samples], 'mc')
tr=lcpq.concat_load([data.samples[sample] for sample in samples], 'tr')
ts=lcpq.concat_load([data.samples[sample] for sample in samples], 'ts')
th=lcpq.concat_load([data.samples[sample] for sample in samples], 'th')

mc2tr=lcpq.concat_load([data.samples[sample] for sample in samples], 'mc2tr')
tr2th=lcpq.concat_load([data.samples[sample] for sample in samples], 'tr2th')

# the single muon
mu=mc[mc.colidx==0]

# merge to get all info in one dataframe

thcnt=tr2th.groupby(['title','evt','from']).size().rename('hitcount')

tracks=mu.merge(
    mc2tr,left_on=['title','evt','colidx'],right_on=['title','evt','from']).merge(
        tr,left_on=['title','evt','to'],right_on=['title','evt','colidx'],suffixes=('_mc','_tr')).merge(
            ts,left_on=['title','evt','sip'],right_on=['title','evt','colidx'], suffixes=('_mc','_tr')).merge(
                thcnt.to_frame(),left_on=['title','evt','colidx_tr'],right_on=['title','evt','from'])

hits=lcpq.map_merge(tr,th,tr2th,suffixes=('_tr','_th'))
hits['tsidx']=hits.sfh+hits.weight.astype(int)
hits=hits.merge(ts,left_on=['title','evt','tsidx'],right_on=['title','evt','colidx'])
hits['pullu']=hits.pou-hits.dze
hits['pullv']=hits.pov-hits.zze

titles=sorted(tracks.title.unique().compute())

# extra variables
tracks['lambda_bin']=tracks['lambda_mc'].map_partitions(pd.cut, bins=np.arange(-3.14/2,3.15/2,0.1))

#
# Plot stuff
histcommon={'histtype':'step', 'density':True}

for title in titles:
    myhits=hits[hits.title==title]

    val=myhits.groupby(['evt','colidx_tr']).size()
    plt.hist(da.compute(val), bins=20, range=(-0.5,19.5), label=title, **histcommon)
plt.legend()
plt.xlabel('Hits on Track')
plt.tight_layout()
plt.savefig('trackhits_count.png')
plt.show()
plt.clf()

for title in titles:
    mytracks=tracks[tracks.title==title]

    df=mytracks.groupby('lambda_bin')[['lambda_mc','hitcount']].mean()
    plt.plot(*da.compute(df['lambda_mc']*180/3.14,df['hitcount']),'.-',label=title)
plt.legend()
plt.xlim(-90,90)
plt.xlabel('Muon $\lambda$ [°]')
plt.ylabel('<Hits on Track>')
plt.tight_layout()
plt.savefig('trackhits_countvslambda.png')
plt.show()
plt.clf()

bins_pull=np.arange(-0.01,0.01,0.001)
hits['pullu_bin']=hits['pullu'].map_partitions(pd.cut, bins=bins_pull)
hits['pullv_bin']=hits['pullv'].map_partitions(pd.cut, bins=bins_pull)

histsv=hits.groupby(['title','system','side','layer','pullv_bin']).size().rename('weight').compute()
histsu=hits.groupby(['title','system','side','layer','pullv_bin']).size().rename('weight').compute()

for (system,side,layer),df in histsv.groupby(['system','side','layer']):
    # hit position pull, v
    for title in titles:
        mydf=df.loc[title]
        plt.hist(bins_pull[:-1], bins=bins_pull, weights=mydf, label=title, **histcommon)
    plt.legend()
    plt.title(f'system={system}, side={side}, layer={layer}')
    plt.tight_layout()
    plt.savefig(f'trackhits_pullv_system{system}_side{side}_layer{layer}.png')
    plt.show()
    plt.clf()
