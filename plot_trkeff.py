#
# Plot tracking efficiency as a function of random variables

#%% Import packages
import mcc.lcparquet as lcpq
import data.dm as dm

import matplotlib.pyplot as plt
import dask.array as da

#%% Load data definitions
data=dm.DataManager('data.yaml')

#%% Load all necessary dataframes
samples=['marlintruth','actstruth']

trackperf=lcpq.concat_load([data[sample] for sample in samples], 'trackperf')
trackperf=lcpq.define_mc(trackperf,'mc_')
trackperf['goodMatch']=trackperf.match>0.5

#%%
#
# Plot stuff
histcommon={'histtype':'step'}

#%% Eff vs pT
for name in samples:
    dsid  =data[name].dsid
    mymu  =trackperf[trackperf.dsid==dsid]
    mymutr=trackperf[(trackperf.dsid==dsid)&(trackperf.goodMatch)]

    den,bins=da.histogram(mymu  .mc_mot, bins=20, range=(0,10))
    num,_   =da.histogram(mymutr.mc_mot, bins=20, range=(0,10))
    num,den =da.compute(num,den)

    eff=da.nan_to_num(num/den)

    plt.hist(bins[:-1], bins, weights=eff, label=data[name].title, **histcommon)
plt.xlabel('Muon $p_{T}$ [GeV]')
plt.ylabel('Efficiency')
#plt.xlim(0,10)
plt.ylim(0.0,1.01)
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('eff_pt.png')
plt.show()
plt.clf()

#%% Eff vs lambda
for name in samples:
    dsid  =data[name].dsid
    mymu  =trackperf[trackperf.dsid==dsid]
    mymutr=trackperf[(trackperf.dsid==dsid)&(trackperf.goodMatch)]

    den,bins=da.histogram(mymu  .mc_lam, bins=100, range=(-3.14/2,3.14/2))
    num,_   =da.histogram(mymutr.mc_lam, bins=100, range=(-3.14/2,3.14/2))
    num,den=da.compute(num,den)

    eff=da.nan_to_num(num/den)

    plt.hist(bins[:-1], bins, weights=eff, label=data[name].title, **histcommon)
plt.xlabel('Muon $\lambda$')
plt.ylabel('Efficiency')
#plt.xlim(0,10)
plt.ylim(0.0,1.01)
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('eff_lambda.png')
plt.show()
plt.clf()

#%% Eff vs lambda
for name in samples:
    dsid  =data[name].dsid
    mymu  =trackperf[trackperf.dsid==dsid]
    mymutr=trackperf[(trackperf.dsid==dsid)&(trackperf.goodMatch)]

    den,bins=da.histogram(mymu  .mc_phi, bins=100, range=(-3.14,3.14))
    num,_   =da.histogram(mymutr.mc_phi, bins=100, range=(-3.14,3.14))
    num,den=da.compute(num,den)

    eff=da.nan_to_num(num/den)

    plt.hist(bins[:-1], bins, weights=eff, label=data[name].title, **histcommon)
plt.xlabel('Muon $\phi$')
plt.ylabel('Efficiency')
#plt.xlim(0,10)
plt.ylim(0.0,1.01)
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('eff_phi.png')
plt.show()
plt.clf()
