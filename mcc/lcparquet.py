import dask
import dask.dataframe as dd
import dask.array as da

import matplotlib.pyplot as plt

import numpy as np

# Detector Systems
systems={}
systems[1]='Vertex (Barrel)'
systems[2]='Vertex (EndCap)'
systems[3]='Inner (Barrel)'
systems[4]='Inner (EndCap)'
systems[5]='Outer (Barrel)'
systems[6]='Outer (EndCap)'


# Track State locations
AtOther = 0
AtIP = 1
AtFirstHit = 2
AtLastHit = 3
AtCalorimeter = 4
AtVertex = 5

# Offsets not handled by dask currently
BitSystem=0
BitSide=5
BitLayer=7

def map_merge(df_left, df_right, df_map, suffixes):
    """
    Merge two dataframes using an intermediatary to/from dataframe.

    The merging columns are `dsid`, `evt` and `colidx`. The left frame
    joins `colidx` to `to` and right frame joins `colidx` to `from`.
    """
    df=df_left.merge(
        df_map,left_on=['dsid','evt','colidx'],right_on=['dsid','evt','from'])
    df=df.merge(
        df_right,left_on=['dsid','evt','to'],right_on=['dsid','evt','colidx'],suffixes=suffixes)
    return df

class LCParquet:
    """
    Sample loader for output files produced using the Marlin LCParquet package
    into Dask dataframes.

    The Parquet file corresponding to a given dataframe can be loaded using
    the `load` function.

    There are a few helpful load functions that load dataframes corresponding
    to the default LCParquet processor. They are named `load_{name}`, where 
    name is dataframe name. In addition to calling `load`, they defined a few
    extra columns. For example, `load_mc` can be used for loading the MC particle
    output file (`mc.parquet`). It also adds the `mot` column corresponding to
    the transverse momentum (among others). The extra definitions are handled
    by the `define_{name}` functions in this module. They are not part of the
    `LCParquet` class, which allows them to be used for outside dataframes
    with the same column names.

    Samples with multiple output files for the same dataframe (ie: result
    of parallel processing) can be specified by giving the constructor
    path wildcards.
    """
    def __init__(self, dsid, path):
        """
        Parameters:
         - path (str): path containing LCParquet's output, wildcards allowed
         - dsid (int): DSID to associate with the sample.
        """
        self.path=path
        self.dsid=dsid

    def load(self,name):
        """
        Load the contents of `path/name.parquet` where `path` is the sample path
        given in `__init__` and `name` is the requested dataframe.
        """
        df=dd.read_parquet('{}/{}.parquet'.format(self.path, name), index='evt')
        df['dsid']=self.dsid
        return df

    def load_mc(self):
        mc=self.load('mc')

        mc=define_mc(mc)

        return mc

    def load_tr(self):
        tr=self.load('track')

        tr=define_tr(tr)

        return tr

    def load_ts(self, Bz=3.57):
        ts=self.load('trackstate')

        ts['mot'      ]=da.fabs(0.3*Bz/ts.ome/1000)
        ts['covmotmot']=(ts.mot/ts.ome)**2 *ts.covomeome
        ts['lambda'   ]=da.arctan(ts.tnl)
        ts['covlamlam']=1/(1+ts.tnl**2)**2 *ts.covtnltnl

        return ts

    def load_th(self):
        th=self.load('trackerhit')

        th['por']=da.sqrt(th.pox*th.pox+th.poy*th.poy)

        th['system'] = da.bitwise_and(th.ci0.map_partitions(lambda col: np.right_shift(col,BitSystem), meta=(None, int)), 0x1f);
        th['side'  ] = da.bitwise_and(th.ci0.map_partitions(lambda col: np.right_shift(col,BitSide  ), meta=(None, int)), 0x03);
        th['layer' ] = da.bitwise_and(th.ci0.map_partitions(lambda col: np.right_shift(col,BitLayer ), meta=(None, int)), 0x3f);

        return th

    def load_st(self):
        st=self.load('simtrackerhit')

        st['por']=da.sqrt(st.pox*st.pox+st.poy*st.poy)

        st['system'] = st.ci0 & 0x1f<<BitSystem;
        st['side'  ] = st.ci0 & 0x03<<BitSide  ;
        st['layer' ] = st.ci0 & 0x3f<<BitLayer ;

        return st
    
    def load_tr2th(self):
        tr2th=self.load('tr2th')
        tr2th=tr2th.drop('colidx', axis=1)

        return tr2th

    def load_mc2tr(self):
        mc2tr=self.load('mc2tr')
        mc2tr=mc2tr.drop('colidx', axis=1)

        return mc2tr

def concat_load(trees, branchset):
    """
    Load a dataframe from multiple samples into one.

    Parameters:
        - trees: List of samples
        - branchset: Dataframe to load (ie: calls `LCParquet.load_{branchset}`)
    """
    load_func=f'load_{branchset}'
    if hasattr(trees[0],load_func):
        mrg=[getattr(tree,load_func)(tree) for tree in trees]
    else:
        mrg=[tree.load(branchset) for tree in trees]
    return dd.concat(mrg)

def has_columns(df, columns):
    """
    Checks if all columns specified by `columns` are
    present inside df.
    """
    return all([col in df.columns for col in columns])

def define_mc(mc, prefix=''):
    """
    Add helpful definitions to a dataframe containing
    MC particles.

    Prameters:
     - mc (DataFrame): dataframe with MC particles
     - prefix (str): Prefix for column names containg MC varables
    """
    # Column names
    mox=f'{prefix}mox'
    moy=f'{prefix}moy'
    moz=f'{prefix}moz'
    mot=f'{prefix}mot'
    mom=f'{prefix}mom'

    tnl=f'{prefix}tnl'
    lam=f'{prefix}lam'
    phi=f'{prefix}phi'

    vtx=f'{prefix}vtx'
    vty=f'{prefix}vty'
    vtz=f'{prefix}vtz'
    vtr=f'{prefix}vtr'

    epx=f'{prefix}epx'
    epy=f'{prefix}epy'
    epz=f'{prefix}epz'
    epr=f'{prefix}epr'

    # Calculations
    if has_columns(mc, [mox,moy]):
        mc[mot]=da.sqrt(mc[mox]*mc[mox]+mc[moy]*mc[moy])
    mc[mom]=da.sqrt(mc[mox]*mc[mox]+mc[moy]*mc[moy]+mc[moz]*mc[moz])
    mc[tnl]=mc[moz]/mc[mot]
    mc[lam]=da.arctan2(mc[moz],mc[mot])
    mc[phi]=da.arctan2(mc[moy],mc[mox])

    # Start and end points
    if has_columns(mc, [vtx,vty]):
        mc[vtr]=da.sqrt(mc[vtx]*mc[vtx]+mc[vty]*mc[vty])
    if has_columns(mc, [epx,epy]):
        mc[epr]=da.sqrt(mc[epx]*mc[epx]+mc[epy]*mc[epy])

    return mc

def define_tr(tr, prefix=''):
    """
    Add helpful definitions to a dataframe containing tracks

    Prameters:
     - tr (DataFrame): dataframe with tracks
     - prefix (str): Prefix for column names containg track varables
    """
    ch2  =f'{prefix}ch2'
    ndf  =f'{prefix}ndf'
    rchi2=f'{prefix}rchi2'

    tr[rchi2]=tr[ch2]/tr[ndf]

    return tr

def plot_hits(hits, st=None, axis='xy'):
    # Axes configuration
    if axis=='xy':
        po0='pox'
        po1='poy'
        xlabel='x [mm]'
        ylabel='y [mm]'
    elif axis=='zr':
        po0='poz'
        po1='por'
        xlabel='z [mm]'
        ylabel='r [mm]'
    else:
        raise "Valid axis: xy or zr"

    # Outline of detector
    if st is not None:
        if axis=='xy':
            isbarrel=st.system.isin([1,3,5])
            plt.plot(*da.compute(st[isbarrel].pox,st[isbarrel].poy),',',color='lightgray')
        else:
            plt.plot(*da.compute(st.poz,st.por),',',color='lightgray')

    # Hits
    plt.plot(*da.compute(hits[po0],hits[po1]),'x')

    # # Particle
    # vtx=0.000000
    # vty=0.000000
    # vtz=0.000000
    # epx=-1618.194274
    # epy=-522.306497
    # epz=-1724.378671
    # mot=1.117851
    # R=1000*mot/(0.3*3.57)

    # t=np.arange(0,1,0.1)
    # plt.plot(R*np.sin(t)+vtx,R*np.cos(t)+vty,'-')
    
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.tight_layout()
    plt.show()


def plot_st_mc_zr(mcidx, st, mc, evt=0):
    themu=mc[mc.colidx==mcidx].loc[evt].compute()
    thest=st[st.mcidx ==mcidx].loc[evt].compute()
    print(themu)
    print(thest)
    plt.plot(st.poz.compute(),st.por.compute(),',',color='lightgray')
    plt.plot([themu.vtz,themu.epz],[themu.vtr,themu.epr],'-b',label='Start to End')
    plt.plot(thest.poz,thest.por,'*',label='Simulated Hits')
    plt.legend(title='Photon')
    plt.xlabel('z [mm]')
    plt.ylabel('r [mm]')
    plt.tight_layout()
    plt.show()

def plot_st_mc_xy(mcidx, st, mc, evt=0):
    themu=mc[mc.colidx==mcidx].loc[evt].compute()
    thest=st[st.mcidx ==mcidx].loc[evt].compute()
    print(themu)
    print(thest)
    isbarrel=st.system.isin([1,3,5])
    plt.plot(*da.compute(st[isbarrel].pox,st[isbarrel].poy),',',color='lightgray')
    plt.plot(st[isbarrel].pox.compute(),st[isbarrel].poy.compute(),',',color='lightgray')
    plt.plot([themu.vtx,themu.epx],[themu.vtx,themu.epy],'-b',label='Start to End')
    plt.plot(thest.pox,thest.poy,'*',label='Simulated Hits')
    plt.legend(title='Photon')
    plt.xlabel('x [mm]')
    plt.ylabel('y [mm]')
    plt.tight_layout()
    plt.show()

def plot_mc_chain(mc, st=None, axis='xy'):
    # Axes configuration
    if axis=='xy':
        ep0='epx'
        ep1='epy'
        vt0='vtx'
        vt1='vty'
        xlabel='x [mm]'
        ylabel='y [mm]'
    elif axis=='zr':
        ep0='epz'
        ep1='epr'
        vt0='vtz'
        vt1='vtr'
        xlabel='z [mm]'
        ylabel='r [mm]'
    else:
        raise "Valid axis: xy or zr"

    # Colors and labels
    colormap={
         22:'blue',
         11:'red'
    }
    labelmap={
         22:'$\gamma$',
         11:'e'
    }

    # Outline of detector
    if st is not None:
        if axis=='xy':
            isbarrel=st.system.isin([1,3,5])
            plt.plot(*da.compute(st[isbarrel].pox,st[isbarrel].poy),',',color='lightgray')
        else:
            plt.plot(*da.compute(st.poz,st.por),',',color='lightgray')

    # MC chain
    for pdg in da.fabs(mc.pdg).unique():
        pdg=int(pdg)
        pmc=mc[da.fabs(mc.pdg)==pdg]
        l=len(pmc.index)
        label=labelmap.get(pdg,pdg)
        labels=label if l==1 else [label]+[None]*(l-1)

        plt.plot(*da.compute([pmc[vt0],pmc[ep0]],[pmc[vt1],pmc[ep1]]),'-',color=colormap.get(pdg, 'black'),label=labels)
    plt.legend(title='PDG ID')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.tight_layout()
    plt.show()

def mc_chain(mcidx, mc):
    mymc=mc[mc.colidx==mcidx].compute()

    thechain=[mymc]    
    for idx,row in mymc.iterrows():
        if row['da0']>=0:
            thechain+=[mc_chain(row['da0'], mc)]
        if row['da1']>=0:
            thechain+=[mc_chain(row['da1'], mc)]
        if row['da2']>=0:
            thechain+=[mc_chain(row['da2'], mc)]
        if row['da3']>=0:
            thechain+=[mc_chain(row['da3'], mc)]
        if row['da4']>=0:
            thechain+=[mc_chain(row['da4'], mc)]
    return dd.concat(thechain)
