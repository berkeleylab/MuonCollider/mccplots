from mcc import lcparquet as lcpq
import data.dm as dm

import matplotlib.pyplot as plt
import dask.array as da
import numpy as np

data=dm.DataManager('data.yaml')

t=data.samples['bib']

st=t.load_st()
th=t.load_th()

histcommon={'density':True, 'histtype':'step'}

#
# Plot layer occupancies

# Barrel
for side in st.side.unique():
    ticklayers=[]

    # Simualted hits
    st_side=st[st.side==side]

    simgroup=st_side.groupby(['system','layer']).size().compute()

    for system, ssh in simgroup.groupby('system'):
        layer=np.right_shift(ssh.index.get_level_values(level=1), lcpq.BitLayer)
        plt.bar(ssh.index.map(str),ssh.values,color='lightgray')
        ticklayers+=list(layer)

    # Digitized hits
    th_side=th[th.side==side]

    hitgroup=th_side.groupby(['system','layer']).size().compute()

    for system, sth in hitgroup.groupby('system'):
        plt.bar(sth.index.map(str),sth.values,label=lcpq.systems.get(system,system))

    plt.xticks(range(len(ticklayers)),labels=ticklayers)
    plt.xlabel('Layer')
    plt.ylabel('Hits / Event')
    plt.ylim(0,1.5e6)
    #plt.ylim(0,1.5e4)
    plt.title(f'{t.title}; $\sigma_t^{{VXD}} = 30$ ps, $\sigma_t^{{IT, OT}} = 60$ ps, [$-3\sigma_t, +5\sigma_t$], $\sqrt{{s}}=1.5$ TeV')
    plt.legend()
    plt.tight_layout()
    plt.savefig(f'occ_{t.title}_side{side}.png')
    plt.show()

    # Table for densities
    lay_group=st_side.groupby(['system','layer'])

    if side==0: #barrel
        lay_r=lay_group.por.mean()
        lay_z=lay_group.poz.max()-lay_group.poz.min()
        lay_A=2*3.14*lay_z*lay_r
    else:
        lay_r1=lay_group.por.max()
        lay_r0=lay_group.por.min()
        lay_A=3.14*((lay_r1)**2-(lay_r0)**2)
    lay_A=lay_A.compute()

    print((hitgroup/lay_A))
    # print(simgroup)
    # print(lay_A)
