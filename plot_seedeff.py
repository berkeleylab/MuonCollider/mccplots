#
# Plot tracking efficiency as a function of random variables

import mcc.lcparquet as lcpq
import data.dm as dm

import matplotlib.pyplot as plt
import dask.array as da

data=dm.DataManager('data.yaml')

#
# Load all necessary dataframes
samples=['actsseed0','actsseed1','actsseed2']

mc=lcpq.concat_load([data.samples[sample] for sample in samples], 'mc')
tr=lcpq.concat_load([data.samples[sample] for sample in samples], 'tr')
ts=lcpq.concat_load([data.samples[sample] for sample in samples], 'ts')

mc2tr=lcpq.concat_load([data.samples[sample] for sample in samples], 'mc2tr')

# the single muon
mu=mc[mc.colidx==0]

# need at least two good hits
mc2tr=mc2tr[mc2tr.weight>0.5]

# merge to get all info in one dataframe
tracks=mu.merge(
    mc2tr,left_on=['title','evt','colidx'],right_on=['title','evt','from']).merge(
        tr,left_on=['title','evt','to'],right_on=['title','evt','colidx']).merge(
            ts,left_on=['title','evt','sfh'],right_on=['title','evt','colidx'], suffixes=('_mc','_tr'))

#
# Plot stuff
histcommon={'histtype':'step'}

# Eff vs pT
for title in tracks.title.unique():
    mymu  =mu    [mu    .title==title]
    mymutr=tracks[tracks.title==title]
    den,bins=da.histogram(mymu  .mot   , bins=15, range=(0,10))
    num,_   =da.histogram(mymutr.mot_mc, bins=15, range=(0,10))
    num,den =da.compute(num,den)
    eff=da.nan_to_num(num/den)

    plt.hist(bins[:-1], bins, weights=eff, label=title, **histcommon)
plt.xlabel('Muon $p_{T}$ [GeV]')
plt.ylabel('Efficiency')
#plt.xlim(0,10)
#plt.ylim(0.8,1.01)
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('eff_pt.png')
plt.show()
plt.clf()

# Eff vs lambda
for title in tracks.title.unique():
    mymu  =mu    [mu    .title==title]
    mymutr=tracks[tracks.title==title]
    den,bins=da.histogram(mymu  ['lambda']   , bins=100, range=(-3.14/2,3.14/2))
    num,_   =da.histogram(mymutr['lambda_mc'], bins=100, range=(-3.14/2,3.14/2))
    num,den=da.compute(num,den)
    eff=da.nan_to_num(num/den)

    plt.hist(bins[:-1], bins, weights=eff, label=title, **histcommon)
plt.xlabel('Muon λ')
plt.ylabel('Efficiency')
#plt.xlim(0,10)
#plt.ylim(0.8,1.01)
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('eff_lambda.png')
plt.show()
plt.clf()

# Eff vs phi
for title in tracks.title.unique():
    mymu  =mu    [mu    .title==title]
    mymutr=tracks[tracks.title==title]
    den,bins=da.histogram(mymu  ['phi']   , bins=100, range=(-3.14,3.14))
    num,_   =da.histogram(mymutr['phi_mc'], bins=100, range=(-3.14,3.14))
    num,den=da.compute(num,den)
    eff=da.nan_to_num(num/den)

    plt.hist(bins[:-1], bins, weights=eff, label=title, **histcommon)
plt.xlabel('Muon ϕ')
plt.ylabel('Efficiency')
#plt.xlim(0,10)
#plt.ylim(0.8,1.01)
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('eff_phi.png')
plt.show()
plt.clf()
