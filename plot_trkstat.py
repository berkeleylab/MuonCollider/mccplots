#
# Plot tracking efficiency as a function of random variables

#%% Import packages
import mcc.lcparquet as lcpq
import data.dm as dm

import matplotlib.pyplot as plt
import dask.array as da

#%% Load data definitions
data=dm.DataManager('data.yaml')

#%% Load all necessary dataframes
samples=['marlintruth','actstruth']

trackperf=lcpq.concat_load([data[sample] for sample in samples], 'trackperf')
trackperf=lcpq.define_mc(trackperf,'mc_')
trackperf=lcpq.define_tr(trackperf,'tr_')
trackperf['goodMatch']=trackperf.match>0.5

#%%
#
# Plot stuff
histcommon={'histtype':'step', 'density':True}

#%% Number of tracks
n=trackperf[trackperf.trackOK].groupby(['dsid','evt']).size().rename('ntrk').reset_index()
for name in samples:
    dsid=data[name].dsid
    myn =n[n.dsid==dsid]

    h,bins=da.histogram(myn.ntrk, bins=500, range=(-0.5, 499999.5))

    plt.hist(bins[:-1], bins, weights=h.compute(), label=data[dsid].title, **histcommon)
plt.xlabel('Number of Tracks')
plt.ylabel('a.u.')
#plt.xlim(0,10)
#plt.ylim(0.0,1.01)
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig('tracks_count.png')
plt.show()
plt.clf()

#%% Reduced chi2
for name in samples:
    dsid  =data[name].dsid
    mytr=trackperf[(trackperf.dsid==dsid)&(trackperf.trackOK)]

    h,bins=da.histogram(mytr.tr_rchi2, bins=100, range=(0,5))

    plt.hist(bins[:-1], bins, weights=h.compute(), label=data[dsid].title, **histcommon)
plt.xlabel('$\chi^2 / nDF$')
plt.ylabel('a.u.')
plt.xlim(0,2)
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig('tracks_rchi2.png')
plt.show()
plt.clf()

#%% Number degrees of Freedom
for name in samples:
    dsid  =data[name].dsid
    mytr=trackperf[(trackperf.dsid==dsid)&(trackperf.trackOK)]

    h,bins=da.histogram(mytr.tr_ndf, bins=50, range=(-0.5,49.5))

    plt.hist(bins[:-1], bins, weights=h.compute(), label=data[dsid].title, **histcommon)
plt.xlabel('Degrees of Freedom')
plt.ylabel('a.u.')
plt.xlim(0,50)
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig('tracks_ndf.png')
plt.show()
plt.clf()

#%% Number of hits per track
for name in samples:
    dsid  =data[name].dsid
    mytr=trackperf[(trackperf.dsid==dsid)&(trackperf.trackOK)]

    h,bins=da.histogram(mytr.tr_nhit, bins=20, range=(-0.5,19.5))

    plt.hist(bins[:-1], bins, weights=h.compute(), label=data[dsid].title, **histcommon)
plt.xlabel('Hits Per Track')
plt.ylabel('a.u.')
plt.xlim(0,50)
plt.legend(loc='upper right')
plt.tight_layout()
plt.savefig('tracks_nhits.png')
plt.show()
plt.clf()
